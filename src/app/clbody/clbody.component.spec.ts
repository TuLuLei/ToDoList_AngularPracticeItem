import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CLBodyComponent } from './clbody.component';

describe('CLBodyComponent', () => {
  let component: CLBodyComponent;
  let fixture: ComponentFixture<CLBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CLBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CLBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TLBodyComponent } from './tlbody.component';

describe('TLBodyComponent', () => {
  let component: TLBodyComponent;
  let fixture: ComponentFixture<TLBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TLBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TLBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

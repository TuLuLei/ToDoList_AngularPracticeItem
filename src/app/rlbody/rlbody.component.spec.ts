import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RLBodyComponent } from './rlbody.component';

describe('RLBodyComponent', () => {
  let component: RLBodyComponent;
  let fixture: ComponentFixture<RLBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RLBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RLBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

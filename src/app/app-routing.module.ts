import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import{RLBodyComponent} from './rlbody/rlbody.component';
import{TLBodyComponent} from './tlbody/tlbody.component';
import{CLBodyComponent} from './clbody/clbody.component';

const routes:Routes = [
  {path:"ToBe",component:TLBodyComponent},
  {path:"Complete",component:CLBodyComponent},
  {path:"All",component:RLBodyComponent},
  {path:"",redirectTo:"/All",pathMatch:"full"}
]

@NgModule({
  exports: [RouterModule],
  imports:[RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
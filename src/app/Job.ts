export class Job {
    id: number;
    jobName: string;
    state: boolean;
}
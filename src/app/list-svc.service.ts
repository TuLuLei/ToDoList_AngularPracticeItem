import { Injectable } from '@angular/core';
import {Data} from './Data'
import {Job} from './Job'

@Injectable({
  providedIn: 'root'
})
export class ListSvcService {

  constructor() { }

  /**
   * 获取全部任务
   */
  getAll(){
    return Data.JobList;
  }

  /**
   * 获取待完成的任务
   */
  getToBe(){
    return Data.JobList.filter(m=>!m.state);
  }

  /**
   * 获取已完成的任务
   */
  getComplete(){
    return Data.JobList.filter(m=>m.state);
  }

  /**
   * 删除任务
   * @param id 要删除任务的ID
   */
  remove(id:number){
    if(id==-1){
      Data.JobList = [];
      return;
    }
    for (let index = 0; index < Data.JobList.length; index++) {
      if (id == Data.JobList[index].id) {
        Data.JobList.splice(index,1);
        break;
      }
    }
  }

  /**
   * 获取数据量
   * @param mode 模式
   */
  getCount(mode:number){
    if(mode==0){
      return Data.JobList.length;
    }else if(mode==1){
      return Data.JobList.filter(m=>!m.state).length;
    }else if(mode==2){
      return Data.JobList.filter(m=>m.state).length;
    }
  }

  /**
   * 添加工作
   * @param value 要添加工作的名
   */
  addJob(value:string){
    let emm = new Job();
    emm.id = Data.IdCount;
    Data.IdCount++;
    emm.state = false;
    emm.jobName = value;
    Data.JobList.push(emm);
  }

}

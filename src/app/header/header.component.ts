import { Component, OnInit } from '@angular/core';
import { ListSvcService } from '../list-svc.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private e: ListSvcService) { }

  ngOnInit() {
  }

  inputJob: string = "";

  onEnter(): void {
    if (this.inputJob == "")
      return;
    this.e.addJob(this.inputJob);
    this.inputJob = "";
  }

}

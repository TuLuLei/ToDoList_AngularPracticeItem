import { Component, OnInit } from '@angular/core';
import {ListSvcService} from '../list-svc.service'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private e:ListSvcService) { }

  ngOnInit() {
  }

  mode = 0;

  change(mode:number){
    this.mode = mode;
  }

  clear(){
    this.e.remove(-1);
  }

}
